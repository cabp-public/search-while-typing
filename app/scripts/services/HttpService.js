class HttpService {
  constructor(container, options) {
    this.container = container;
    this.options = options;

    this.get = this.get.bind(this);
  }

  get(url, callback, data) {
    let fullUrl = this.options['protocol'] + '://' + this.options['host'];
    fullUrl += ':' + this.options['port'];
    fullUrl += this.options['baseUrl'] + url;

    fetch(fullUrl, {
      method: 'GET',
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(data)
      }
    })
      .then((response) => {
        if ( !response.ok ) {
          throw Error(response.statusText);
        }

        return response.json();
      })
      .then((json) => {
        callback !== false && callback(json);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  post(url, callback, data) {
    let fullUrl = this.options['protocol'] + '://' + this.options['host'];
    fullUrl += ':' + this.options['port'];
    fullUrl += this.options['baseUrl'] + url;

    fetch(fullUrl, {
      method: 'POST',
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(data)
      }
    })
      .then((response) => {
        if ( !response.ok ) {
          throw Error(response.statusText);
        }

        return response.json();
      })
      .then((json) => {
        callback !== false && callback(json);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  put() {}

  delete() {}
}

export default HttpService;