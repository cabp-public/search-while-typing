import React, { Component } from 'react';

import ResultCard from './resultCard';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: {
        results: []
      }
    };
  }

  componentWillReceiveProps(items) {
    this.setState({ items });
  }

  render() {
    const { items } = this.state;
    const { results } = items;

    return (
      <section id="home">
        <div className="content">
          {results.map(item => {
              return (
                <ResultCard key={ item._id } item={ item } />
              );
            }
          )}
        </div>
      </section>
    );
  }


}

// Export out the React Component
module.exports = Home;