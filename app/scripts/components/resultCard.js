import React, { Component } from 'react';

class ResultCard extends Component {
  constructor(props) {
    super(props);

    this.state = { item: this.props.item };
  }

  render() {
    const { item } = this.state;

    return (
      <div class="itemCard">
        <h4>{ item.name } ({ item.price })</h4>
        <img src={item.picture} />
        <p>{ item.about }</p>
      </div>
    );
  }


}

// Export out the React Component
module.exports = ResultCard;
