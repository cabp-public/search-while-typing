import React, { Component } from 'react';

class Menu extends Component {
  constructor() {
    super();

    this.onSearchHandler = null;
    this.onSearchReady = null;
    this.state = {
      showingSearch: false
    };

    this.onSearch = this.onSearch.bind(this);
  }

  componentDidMount() {
    this.onSearchHandler = this.props['onSearchHandler'];
    this.onSearchReady = this.props['onSearchReady'];
  }

  showSearchContainer(e) {
    e.preventDefault();
    this.setState({
      showingSearch: !this.state.showingSearch
    });
  }

  onSearch(e) {
    this.onSearchHandler(e, this.onSearchReady);
  }

  render() {
    return (
      <header className="menu">
        <div className="menu-container">
          <div className="menu-holder">
            <h1>ELC</h1>
            <nav>
              <a href="#" className="nav-item">HOLIDAY</a>
              <a href="#" className="nav-item">WHAT'S NEW</a>
              <a href="#" className="nav-item">PRODUCTS</a>
              <a href="#" className="nav-item">BESTSELLERS</a>
              <a href="#" className="nav-item">GOODBYES</a>
              <a href="#" className="nav-item">STORES</a>
              <a href="#" className="nav-item">INSPIRATION</a>

              <a href="#" onClick={(e) => this.showSearchContainer(e)}>
                <i className="material-icons search">search</i>
              </a>
            </nav>
          </div>
        </div>
        <div className={(this.state.showingSearch ? "showing " : "") + "search-container"}>
          <input type="text" onChange={ this.onSearch } />
          <a href="#" onClick={(e) => this.showSearchContainer(e)}>
            <i className="material-icons close">close</i>
          </a>
        </div>
      </header>
    );
  }


}

// Export out the React Component
module.exports = Menu;