import React, { Component } from 'react';

import Menu from './components/menu';
import Home from './components/home';


/**
 * We can start our initial App here in the main.js file
 */
class App extends Component {
  constructor(props) {
    super(props);

    this.contentController = null;
    this.state = {
      moduleReady: false,
      results: []
    };

    this.onSearchReady = this.onSearchReady.bind(this);
  }

  componentDidMount() {
    const moduleReady = true;

    this.contentController = this.props['container']['controllers']['ContentController'];
    this.setState({ moduleReady });
  }

  onSearchReady(data) {
    const results = data;
    this.setState({ results });
  }

  render() {
    return this.state.moduleReady === true ? (
      <div className="App">
        <Menu
          onSearchHandler={ this.contentController.onSearch }
          onSearchReady={ this.onSearchReady }
        />
        <Home results={this.state.results} />
      </div>
    ) : null;
  }
}

export default App;
