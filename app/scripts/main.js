import React from 'react';
import { render } from 'react-dom';

import AppContainer from './classes/AppContainer';
import App from "./app";

const Container = new AppContainer();

document.addEventListener(AppContainer.initEvent, function (event) {
  render(<App container={Container} />, document.getElementById('root'));
});

Container.init();
