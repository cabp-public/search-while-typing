class ContentController {
  constructor(container, options) {
    this.container = container;
    this.options = options;

    this.onSearch = this.onSearch.bind(this);
  }

  // We're not using redux, hence the callback...
  onSearch(event, callback) {
    const { target } = event;
    const keywords = target.value.trim().split(' ');
    const { HttpService } = this.container['services'];

    HttpService.post('', callback, keywords);
  }
}

export default ContentController;
